'use strict'

function Person(id, name, age, gender){
    this.id = id;
    this.name = name;
    this.age = age;
    this.gender = gender;
}

class Person {
    constructor(id, name, age, gender){
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
    }
    exit() {
        console.log(`Person ${this.name}, left away`);
    }   
}

const man = new Person(1, 'Max', 24, 'Man');

Person.prototype.exit = function(){
    console.log(`Person ${this.name}, left away`)
}

man.exit;