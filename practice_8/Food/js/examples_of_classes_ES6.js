'use strict'

class Rectangle {
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }
    calcArea() {
        return this.width * this.height;
    }
}

const square = new Rectangle(2, 2);
