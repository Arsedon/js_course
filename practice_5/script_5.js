/*
•	Какое будет выведено значение: let x = 5; alert( x++ ); ?

•	Чему равно такое выражение: [ ] + false - null + true ?

•	Что выведет этот код: let y = 1; let x = y = 2; alert(x); ?

•	Чему равна сумма [ ] + 1 + 2?

•	Что выведет этот код: alert( "1"[0] )?

•	Чему равно 2 && 1 && null && 0 && undefined ?

•	Есть ли разница между выражениями? !!( a && b ) и (a && b)?

•	Что выведет этот код: alert( null || 2 && 3 || 4 ); ?

•	a = [1, 2, 3]; b = [1, 2, 3]; Правда ли что a == b ?

•	Что выведет этот код: alert( +"Infinity" ); ?

•	Верно ли сравнение: "Ёжик" > "яблоко"?

•	Чему равно 0 || "" || 2 || undefined || true || falsе ?
*/

'use strict';

let x = 5; 
alert( x++ ); //5

console.log([] + false - null + true );//NaN

let y = 1; 
let a = y = 2; 
alert(y);

console.log([] + 1 + 2);// 12 сложение 1 и 2 как стринги

alert( "124"[0] ); //1

console.log(2 && 1 && null && 0 && undefined);//null

console.log(!!( a && y ));//сравнение boolean
console.log(( a && y ));//значение переменных 


alert( 2 || 2 && null || 1 );//2

let e = [1, 2, 3]; 
let c = [1, 2, 3]; 

console.log(e == c);//false

alert( +"Infinity" );//Infinity

console.log("Ёжик" < "яблоко");//нецелесообразное мб для сортировки

console.log(4 || "" || 2 || undefined || true);//4









