'use strict'


//fetch returned promise 

fetch('https://jsonplaceholder.typicode.com/posts',
{
    method: 'POST',
    body: JSON.stringify({name: 'Viktor'}),
    headers: {
        'Content-Type': 'application/json'
    }
})
.then(response => response.json())
.then(json => console.log(json));
