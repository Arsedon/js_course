'use strict'

class PersonRequest {
    constructor(firstName, lastName, userEmail) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userEmail = userEmail;
    }
}

const inputs = document.querySelectorAll('input'),
      btn = document.querySelector('button');

btn.addEventListener('click', () => {
    const request = new XMLHttpRequest();
    let firstName;
    let lastName;
    let email;
    
    inputs.forEach(val => {
        if(val.name == 'first_name'){
            firstName = val.value;
        } 
        if(val.name == 'last_name'){
            lastName = val.value;
        } 
        if(val.name == 'user_email'){
            email = val.value;
        }
    })

    request.open('POST', 'http://localhost:8080/response', true);
    request.setRequestHeader('Access-Control-Allow-Origin', '*');
    request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
    
    const person = new PersonRequest(firstName, lastName, email);

    console.log(person);
    console.log(JSON.stringify(person));
    console.log(JSON.parse(JSON.stringify(person)));

    request.send(JSON.stringify(person));

    request.addEventListener('readystatechange', () => {
        if(request.readyState == 4 && request.status == 201){
            console.log(JSON.parse(request.response))
        }
    })
});

