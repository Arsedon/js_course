
'use strict';

function doFirst(){
    setTimeout(function(){
        console.log(1);
    }, 500);
}

function doSecond(){
    console.log(2);
}

doFirst();
doSecond();

function learnJs(lang, callback){
    console.log(`Я учу: ${lang}`);
    callback();
}

learnJs('Java Script', function(){
    console.log('Урок пройден');
});