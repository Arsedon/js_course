'use strict';

const arr = [1,2,4,5,2,5,56,1];
arr.pop();
console.log(arr);

arr.push(11);
console.log(arr);

arr.forEach(function(obj, b, arr){
    console.log(`${obj}: ${b}`);
});

arr.sort(function(a, b){
    return a-b;
});

console.log(arr);
