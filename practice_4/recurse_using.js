
'use strict';

const config = {
    name: 'conf',
    widht: 1024,
    height: 2048,
    colors: {
        border: 'black',
        bg: 'left',
        colorNums: {
            rgb: 11,
            r: 124,
            dontKnow: 555
        },
        someArray:[1,2,4],
    },
};

let result = '';

function recurseLogging(obj){
    
    for (let key in obj){
        if(typeof(obj[key]) === 'object'){
            result += key + ':' + '\n' +'';
            recurseLogging(obj[key]);
        } else{
        result += key + ':' + obj[key] + '\n';
        }
    }
    return result;
}

console.log(recurseLogging(config));